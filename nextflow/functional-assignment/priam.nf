params.refdbDir = "${baseDir}/refdb"
params.refdb = 'priam:JAN18'

include {DownloadRefDb} from '../helper/downloadRefDb.nf' params(refdbDir: params.refdbDir)
include {PriamSearchProc} from './process/priamSearch-proc.nf'
include {Export} from '../helper/export.nf'

workflow Priam {
  take:
    input

  main:
    DownloadRefDb(params.refdb)
    refdb = DownloadRefDb.out.dbPath
    PriamSearchProc(refdb, input)
    genomeECs = PriamSearchProc.out.genomeECs | flatten | collectFile(keepHeader: true, skip: 1) | collectFile(name: 'genomeECs.txt')
    genomeEnzymes = PriamSearchProc.out.genomeEnzymes | flatten | collectFile(keepHeader: true, skip: 1) | collectFile(name: 'genomeEnzymes.txt')
    predictableECs = PriamSearchProc.out.predictableECs | flatten | collectFile(keepHeader: true, skip: 1) | collectFile(name: 'predictableECs.txt')
    sequenceECs = PriamSearchProc.out.sequenceECs | collectFile()
    priam = genomeECs.mix(genomeEnzymes, predictableECs, sequenceECs) | collect
    Export("functionalAssignment/priam", priam)

  emit:
    priam = priam
}
