process Gff {
  label 'functional_assignment'

  container "gff:0.3.0-28-add-gff-output-SNAPSHOT"

  input:
    path contigs, stageAs: 'in/*'
    path mga, stageAs: 'in/*'
    path priam, stageAs: 'in/*'
    path diamond, stageAs: 'in/*'
    path interpro, stageAs: 'in/*'

  output:
    path 'out/features.gff', emit: gff

  shell:
    '''
    set +u
    case $(echo "!{task.memory}" | cut -d' ' -f2) in
         [gG]B*) UNIT=1073741824;;
         [mM]B*) UNIT=1048576;;
         [kK]B*) UNIT=1024;;
         B*) UNIT=1;;
    esac
    MEMORY=$(( $(echo "!{task.memory}" | cut -d '.' -f1 | cut -d ' ' -f1) * $UNIT ))
    set -x
    /opt/docker/bin/gff -J-Xms$((MEMORY/2)) -J-Xmx$MEMORY -XX:ActiveProcessorCount=!{task.cpus} -- \
      --outdir out \
      --fastadir "!{contigs}" \
      --mgaout "!{mga}" \
      --priamout "in/genomeEnzymes.txt" \
      --diamondout "!{diamond}" \
      --interproout "!{interpro}"
    '''
}
