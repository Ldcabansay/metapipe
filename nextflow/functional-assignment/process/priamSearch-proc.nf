params.refdbDir = "${baseDir}/refdb"

process PriamSearchProc {
  label 'functional_assignment'
  tag "$DATUM"

  container 'registry.gitlab.com/uit-sfb/genomic-tools/priamsearch:2.0'
  containerOptions = "-v ${params.refdbDir}:/refdb"

  input:
    val refdb
    tuple DATUM, path(input, stageAs: 'in/*')

  output:
    path "out/slices/${DATUM}/genomeECs/*", emit: genomeECs
    path "out/slices/${DATUM}/genomeEnzymes/*", emit: genomeEnzymes
    path "out/slices/${DATUM}/predictableECs/*", emit: predictableECs
    path "out/slices/${DATUM}/sequenceECs.txt", emit: sequenceECs

  shell:
    '''
    set +u
    case $(echo "!{task.memory}" | cut -d' ' -f2) in
         [gG]B*) UNIT=1073741824;;
         [mM]B*) UNIT=1048576;;
         [kK]B*) UNIT=1024;;
         B*) UNIT=1;;
    esac
    MEMORY=$(( $(echo "!{task.memory}" | cut -d '.' -f1 | cut -d ' ' -f1) * $UNIT ))
    DB_PATH="!{refdb}/db/priam/"
    set -x
    java -Xms$((MEMORY/2)) -Xmx$MEMORY -XX:ActiveProcessorCount=!{task.cpus} -jar /app/priamsearch/PRIAM_search.jar -p "$DB_PATH" -np 1 -pt 0.5 -mp 70 -cc T -cg F -n mp -i "!{input}" --out tmp
    set +x
    #We split each file by section (using the section name as file name)
    mkdir -p out/slices/!{DATUM}
    mv tmp/PRIAM_mp/ANNOTATION/sequenceECs.txt out/slices/!{DATUM}
    files=("genomeECs" "genomeEnzymes" "predictableECs")
    for file in ${files[@]}; do
      mkdir -p "out/slices/!{DATUM}/$file"
      awk '{if ($0 != "") print "#"$0 > "out/slices/!{DATUM}/'"$file"'/" NR}' RS='#' tmp/PRIAM_mp/ANNOTATION/${file}.txt
      for f in "out/slices/!{DATUM}/$file"/*; do
        NAME="$(head -n 1 "$f")"
        mv "$f" "$(dirname "$f")"/"$NAME"
      done
    done
    '''
}