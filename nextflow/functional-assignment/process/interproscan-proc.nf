params.refdbDir = "${baseDir}/refdb"

process InterproscanProc {
  label 'functional_assignment'
  tag "$DATUM"

  container 'registry.gitlab.com/uit-sfb/genomic-tools/interproscan:5.42-78.0'
  containerOptions = "-v ${params.refdbDir}:/refdb"

  ext.toolsCpu = 1
  ext.precalcService = ''

  input:
    val refdb
    tuple DATUM, path(input, stageAs: 'in/*')

  output:
    path "out/slices/${DATUM}/interpro.out", emit: interpro

  shell:
    '''
    set +u
    #The memory setting is not used since most of the memory usage comes from processes spawned outside of the JVM
    MAX_WORKERS=$(( (!{task.cpus} - 1) / !{task.ext.toolsCpu} ))
    if [[ $MAX_WORKERS < 1  ]]; then MAX_WORKERS=1; fi
    #Interproscan expects the tools at this location
    ln -s "!{refdb}/db/interpro" /app/interpro/data
    OUT_DIR="out/slices/!{DATUM}"
    mkdir -p "$OUT_DIR"
    if [[ "!{task.ext.precalcService}" != "default" ]]; then
       if [[ -z "!{task.ext.precalcService}" ]]; then
         echo "Disabled precalc service"
       else
         echo "Using custom service !{task.ext.precalcService}"
       fi
       sed -r -i 's<(precalculated.match.lookup.service.url=).*<\\1!{task.ext.precalcService}<' /app/interpro/interproscan.properties
    else
       echo "Using EBI precalc service"
    fi
    sed -r -i 's<(number\\.of\\.embedded\\.workers=)([0-9]*)<\\1'$MAX_WORKERS'<' /app/interpro/interproscan.properties
    sed -r -i 's<(maxnumber\\.of\\.embedded\\.workers=)([0-9]*)<\\1'$MAX_WORKERS'<' /app/interpro/interproscan.properties
    cat /app/interpro/interproscan.properties
    set -x
    /app/interpro/interproscan.sh -goterms -iprlookup -f tsv --applications TIGRFAM,PRODOM,SMART,ProSiteProfiles,ProSitePatterns,HAMAP,SUPERFAMILY,PRINTS,GENE3D,PIRSF,COILS \
      -i "!{input}" -o "$OUT_DIR/interpro.out" --tempdir tmp/temp
    '''
}