params.refdbDir = "${baseDir}/refdb"
params.sensitivity = 'sensitive'

process DiamondProc {
  label 'functional_assignment'
  tag "$DATUM"

  container 'registry.gitlab.com/uit-sfb/genomic-tools/diamond:0.9.31'
  containerOptions = "-v ${params.refdbDir}:/refdb"

  input:
    val refdb
    tuple DATUM, path(input, stageAs: 'in/*')

  output:
    path "out/slices/${DATUM}/diamond.out", emit: diamond

  shell:
    '''
    set +u
    DB_NAME=$(basename $(dirname "!{refdb}"))
    if [[ "$DB_NAME" == "diamond-uniref50" ]]; then
      DB_SUFFIX=uniprot/uniref50
      OUT_FMT="6"
    else
      DB_SUFFIX=marref/proteins
      OUT_FMT="6 qseqid sseqid stitle pident length mismatch gapopen qstart qend sstart send evalue bitscore"
    fi
    DB_PATH="!{refdb}/db/diamond/$DB_SUFFIX/nr.dmnd"
    OUT_DIR="out/slices/!{DATUM}"
    mkdir -p "$OUT_DIR"
    case "!{params.sensitivity}" in
      sensitive)
        SENSITIVE_FLAG="--sensitive"
        ;;
      more-sensitive)
        SENSITIVE_FLAG="--more-sensitive"
        ;;
      *)
        SENSITIVE_FLAG=""
        ;;
    esac
    case $(echo "!{task.memory}" | cut -d' ' -f2) in
         [gG]B*) UNIT=1073741824;;
         [mM]B*) UNIT=1048576;;
         [kK]B*) UNIT=1024;;
         B*) UNIT=1;;
    esac
    MEMORY=$(( $(echo "!{task.memory}" | cut -d '.' -f1 | cut -d ' ' -f1) * $UNIT ))
    BLOCK_SIZE=$(( $MEMORY / (6 *  1073741824) ))
    if [[ "$BLOCK_SIZE" == "0" ]]; then
      BLOCK_SIZE="0.2"
    fi
    set -x
    #Cf https://github.com/bbuchfink/diamond_docs/blob/master/2%20Command%20line%20options.MD#memory--performance-options
    /app/diamond/diamond blastp -d "$DB_PATH" -q "!{input}" -o "$OUT_DIR/diamond.out" -k 5 --algo 0 --threads !{task.cpus} --index-chunks 4 --block-size $BLOCK_SIZE $SENSITIVE_FLAG -t tmp --outfmt $OUT_FMT
    '''
}