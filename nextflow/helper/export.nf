params.export = '.'

process Export {
  label 'helper'

  publishDir "${params.export}"

  input:
    val s
    path p, stageAs: "out/*"

  output:
    path "${s}"

  "mkdir -p ${s} && mv out/* ${s}"
}
