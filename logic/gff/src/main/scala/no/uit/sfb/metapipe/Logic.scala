package no.uit.sfb.metapipe

import java.io.{FileReader, FileWriter, PrintWriter, Reader}
import java.nio.file.Path

import no.uit.sfb.metapipe.format.{
  DiamondData,
  Gff3Data,
  InterproData,
  MgaData,
  PriamData
}
import no.uit.sfb.scalautils.common.FileUtils
import org.apache.commons.io.IOUtils

object Logic {
  protected def writeToFile(features: Iterator[Gff3Data],
                            fastaPath: Path,
                            filepath: Path): Unit = {
    val fastaSource: Reader = new FileReader(fastaPath.toString)
    val out = new PrintWriter(new FileWriter(filepath.toString))
    try {
      out.println("##gff-version 3")
      features foreach { f =>
        out.println(f.asString)
      }
      out.println("##FASTA")
      IOUtils.copyLarge(fastaSource, out)
    } finally {
      fastaSource.close()
      out.close()
    }
  }

  def exec(config: Config): Unit = {
    val (mga, src) = MgaData.toGff(config.mgaOut)
    val priam = PriamData.addToGff(mga, config.priamOut)
    val diamond = DiamondData.addToGff(priam, config.diamondOut)
    val features = InterproData.addToGff(diamond, config.interproOut)

    FileUtils.createDirs(config.outDir)
    try {
      writeToFile(features,
                  config.fastaDir,
                  config.outDir.resolve("features.gff"))
    } finally {
      src.close()
    }
  }
}
