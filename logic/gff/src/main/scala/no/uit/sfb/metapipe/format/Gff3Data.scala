package no.uit.sfb.metapipe.format

case class Gff3Data(
    seqid: String = "",
    source: String = "",
    featureType: String = "",
    start: String = "",
    end: String = "",
    score: String = "",
    strand: String = "",
    phase: String = "",
    featureAttributes: Seq[(String, String)] = Seq()
) {
  def merge(other: Gff3Data) = {
    copy(
      seqid = if (other.seqid.nonEmpty) other.seqid else seqid,
      source = if (other.source.nonEmpty) other.source else source,
      featureType =
        if (other.featureType.nonEmpty) other.featureType else featureType,
      start = if (other.start.nonEmpty) other.start else start,
      end = if (other.end.nonEmpty) other.end else end,
      score = if (other.score.nonEmpty) other.score else score,
      strand = if (other.strand.nonEmpty) other.strand else strand,
      phase = if (other.phase.nonEmpty) other.phase else phase,
      featureAttributes = featureAttributes ++ other.featureAttributes
    )
  }

  lazy val asString = {
    val attrs = featureAttributes.map { case (k, v) => s"$k=$v" }.mkString(";")
    Seq(
      seqid
        .split("_", 3)
        .dropRight(1)
        .mkString("_"), //We remove the gene number part. Ex: k141_15_gene_1 -> k141_15
      if (source.isEmpty) "." else source,
      if (featureType.isEmpty) "." else featureType,
      if (start.isEmpty) "." else start,
      if (end.isEmpty) "." else end,
      if (score.isEmpty) "." else score,
      if (strand.isEmpty) "." else strand,
      if (phase.isEmpty) "." else phase,
      if (attrs.isEmpty) "." else attrs
    ).mkString("\t")
  }
}
