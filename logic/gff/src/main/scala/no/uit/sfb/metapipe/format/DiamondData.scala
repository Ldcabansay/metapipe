package no.uit.sfb.metapipe.format

import java.nio.file.Path

import scala.io.Source
import scala.util.matching.Regex

case class DiamondData(
    geneId: String,
    sseqId: String,
    geneDescription: String
) {
  lazy val toGff = {
    def parseDiamondName(n: String): String = {
      val myReg = new Regex("(?<=\\[)[^]]+(?=\\])")
      val speciesName = myReg.findFirstIn(n).getOrElse("")
      val featureName =
        n.substring(0, n.indexOf("[")).split(" ").drop(1).mkString(" ")
      featureName + " " + speciesName
    }
    Gff3Data(seqid = geneId,
             featureAttributes =
               Seq(("Diamond_accession", sseqId),
                   ("Diamond_description", parseDiamondName(geneDescription))))
  }
}

object DiamondData {
  protected def fromlist(args: List[String]): Option[DiamondData] = args match {
    case id :: sseqId :: description :: _ :: _ :: _ :: _ :: _ :: _ :: _ :: _ :: _ :: _ =>
      Some(DiamondData(id, sseqId, description))
    case _ => None
  }

  def apply(p: Path): (Iterator[DiamondData], Source) = {
    val source = Source.fromFile(p.toString)
    def processFileDiamond(): Iterator[String] = {
      val res = source
        .getLines()
        .foldLeft[(Iterator[String], String)]((Iterator[String](), null)) {
          case ((acc, last), i)
              if last != null && (i
                .split("\\t")
                .head == last.split("\\t").head) =>
            (acc, i)
          case ((acc, _), i) => (acc ++ Iterator(i), i)
        }
      res._1
    }
    val src = processFileDiamond()
    val it = src.flatMap { l =>
      val p = l.trim.replaceAll(" +", " ").split("\\t").toList
      DiamondData.fromlist(p)
    }
    (it, source)
  }

  def addToGff(data: Iterator[Gff3Data], filepath: Path): Iterator[Gff3Data] = {
    if (filepath == null)
      data
    else {
      val (it, src) = DiamondData(filepath)
      val diamondPart = try {
        it.toVector //we need to materialize the iterator otherwise 'find' would consume it
      } finally {
        src.close()
      }
      data map { gene =>
        diamondPart.find(_.geneId == gene.seqid) match {
          case Some(d) =>
            gene.merge(d.toGff)
          case _ => gene
        }
      }
    }
  }
}
