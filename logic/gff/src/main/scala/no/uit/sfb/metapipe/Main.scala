package no.uit.sfb.metapipe

import java.io.File

import no.uit.sfb.info.gff.BuildInfo
import scopt.OParser

object Main extends App {
  try {
    val builder = OParser.builder[Config]

    val name = BuildInfo.name
    val ver = BuildInfo.version
    val gitCommitId = BuildInfo.formattedShaVersion

    val parser = {
      import builder._
      OParser.sequence(
        programName(name),
        head(name, ver),
        head(s"git: ${gitCommitId.getOrElse("unknown")}"),
        opt[Int]("cpu")
          .action((x, c) => c.copy(cpu = x))
          .text("Number of CPUs available for pigz"),
        opt[File]("outdir").required
          .action((x, c) => c.copy(outDir = x.toPath))
          .text("Output directory"),
        opt[File]("fastadir").required
          .action((x, c) => c.copy(fastaDir = x.toPath))
          .text(""),
        opt[File]("mgaout").required
          .action((x, c) => c.copy(mgaOut = x.toPath))
          .text(""),
        opt[File]("priamout").required
          .action((x, c) => c.copy(priamOut = x.toPath))
          .text(""),
        opt[File]("diamondout").required
          .action((x, c) => c.copy(diamondOut = x.toPath))
          .text(""),
        opt[File]("interproout").required
          .action((x, c) => c.copy(interproOut = x.toPath))
          .text("")
      )
    }

    val check = {
      import builder._
      OParser.sequence(
        checkConfig(c => success)
      )
    }

    OParser.parse(parser ++ check, args, Config()) match {
      case Some(config) =>
        Logic.exec(config)
      case _ =>
        // arguments are bad, error message will have been displayed
        throw new IllegalArgumentException()
    }
  } catch {
    case e: Throwable =>
      println(e.toString)
      println(e.printStackTrace())
      sys.exit(1)
  }
}
